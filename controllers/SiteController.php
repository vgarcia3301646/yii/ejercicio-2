<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Lleva;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionConsulta1ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) resultado"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 1 con Active Record',
            'enunciado' => 'Número de ciclistas que hay',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista;'
        ]);
        
    }
    
    public function actionConsulta1dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => "SELECT COUNT(*) AS resultado FROM ciclista",
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 1 con DAO',
            'enunciado' => 'Número de ciclistas que hay',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista;'
        ]);
        
    }
    
    public function actionConsulta2ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) resultado")->where("nomequipo = 'Banesto'"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 2 con Active Record',
            'enunciado' => 'Número de ciclistas que hay del equipo Banesto',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista WHERE nomequipo = "Banesto";'
        ]);
        
    }
    
    public function actionConsulta2dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 2 con DAO',
            'enunciado' => 'Número de ciclistas que hay del equipo Banesto',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista WHERE nomequipo = "Banesto";'
        ]);
        
    }
    
    public function actionConsulta3ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("avg(edad) resultado"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 3 con Active Record',
            'enunciado' => 'Edad media de los ciclistas',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista;'
        ]);
        
    }
    
    public function actionConsulta3dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 3 con DAO',
            'enunciado' => 'Edad media de los ciclistas',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista;'
        ]);
        
    }
    
    public function actionConsulta4ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("avg(edad) resultado")->where("nomequipo = 'Banesto'"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 4 con Active Record',
            'enunciado' => 'La edad media de los ciclistas de Banesto',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista WHERE nomequipo = "Banesto";'
        ]);
        
    }
    
    public function actionConsulta4dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 4 con DAO',
            'enunciado' => 'La edad media de los ciclistas de Banesto',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista WHERE nomequipo = "Banesto";'
        ]);
        
    }
    
    public function actionConsulta5ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("avg(edad) resultado")->groupBy("nomequipo"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 5 con Active Record',
            'enunciado' => 'La edad media de los ciclistas por cada equipo',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista GROUP BY nomequipo;'
        ]);
        
    }
    
    public function actionConsulta5dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista GROUP BY nomequipo',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 5 con DAO',
            'enunciado' => 'La edad media de los ciclistas por cada equipo',
            'sql' => 'SELECT AVG(edad) AS resultado FROM ciclista GROUP BY nomequipo;'
        ]);
        
    }
    
    public function actionConsulta6ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) resultado")->groupBy("nomequipo"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 6 con Active Record',
            'enunciado' => 'El número de ciclistas por equipo',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista GROUP BY nomequipo;'
        ]);
        
    }
    
    public function actionConsulta6dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista GROUP BY nomequipo',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 6 con DAO',
            'enunciado' => 'El número de ciclistas por equipo',
            'sql' => 'SELECT COUNT(*) AS resultado FROM ciclista GROUP BY nomequipo;'
        ]);
        
    }
    
    public function actionConsulta7ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Puerto::find()->select("count(*) resultado"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 7 con Active Record',
            'enunciado' => 'El número total de puertos',
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto;'
        ]);
        
    }
    
    public function actionConsulta7dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 7 con DAO',
            'enunciado' => 'El número total de puertos',
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto;'
        ]);
        
    }
    
    public function actionConsulta8ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Puerto::find()->select("count(*) resultado")->where("altura > 1500"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 8 con Active Record',
            'enunciado' => 'El número total de puertos mayores de 1500',
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto WHERE altura > 1500;'
        ]);
        
    }
    
    public function actionConsulta8dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto WHERE altura > 1500',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['resultado'],
            'titulo' => 'Consulta 8 con DAO',
            'enunciado' => 'El número total de puertos mayores de 1500',
            'sql' => 'SELECT COUNT(*) AS resultado FROM puerto WHERE altura > 1500;'
        ]);
        
    }
    
    public function actionConsulta9ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo")->groupBy("nomequipo")->having("count(*) > 4"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['nomequipo'],
            'titulo' => 'Consulta 9 con Active Record',
            'enunciado' => 'Listar el nombre de los equipos que tengan más de 4 ciclistas',
            'sql' => 'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(*) > 4;'
        ]);
        
    }
    
    public function actionConsulta9dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(*) > 4',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['nomequipo'],
            'titulo' => 'Consulta 9 con DAO',
            'enunciado' => 'Listar el nombre de los equipos que tengan más de 4 ciclistas',
            'sql' => 'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(*) > 4;'
        ]);
        
    }
    
    public function actionConsulta10ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("nomequipo")->where("edad between 28 and 32")->groupBy("nomequipo")->having("count(*) > 4"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['nomequipo'],
            'titulo' => 'Consulta 10 con Active Record',
            'enunciado' => 'Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32',
            'sql' => 'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4;'
        ]);
        
    }
    
    public function actionConsulta10dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['nomequipo'],
            'titulo' => 'Consulta 10 con DAO',
            'enunciado' => 'Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32',
            'sql' => 'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4;'
        ]);
        
    }
    
    public function actionConsulta11ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal, count(*) resultado")->groupBy("dorsal"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['dorsal', 'resultado'],
            'titulo' => 'Consulta 11 con Active Record',
            'enunciado' => 'Indícame el número de etapas que ha ganado cada uno de los ciclistas',
            'sql' => 'SELECT dorsal, COUNT(*) AS resultado FROM lleva GROUP BY dorsal;'
        ]);
        
    }
    
    public function actionConsulta11dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT dorsal, COUNT(*) AS resultado FROM lleva GROUP BY dorsal',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['dorsal', 'resultado'],
            'titulo' => 'Consulta 11 con DAO',
            'enunciado' => 'Indícame el número de etapas que ha ganado cada uno de los ciclistas',
            'sql' => 'SELECT dorsal, COUNT(*) AS resultado FROM lleva GROUP BY dorsal;'
        ]);
        
    }
    
    public function actionConsulta12ar() {
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Etapa::find()->select("dorsal")->groupBy("dorsal")->having("count(*) > 1"),
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 12 con Active Record',
            'enunciado' => 'Indícame el dorsal de los ciclistas que hayan ganado más de una etapa',
            'sql' => 'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1;'
        ]);
        
    }
    
    public function actionConsulta12dao() {
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1',
        ]);
        
        return $this->render('resultado', [
            'resultados' => $dataProvider,
            'campos' => ['dorsal'],
            'titulo' => 'Consulta 12 con DAO',
            'enunciado' => 'Indícame el dorsal de los ciclistas que hayan ganado más de una etapa',
            'sql' => 'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1;'
        ]);
        
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
